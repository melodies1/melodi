// SPDX-License-Identifier: MIT

import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";

pragma solidity ^0.8.20;
// contract MusicNFT is ERC721URIStorage{
//     uint public _tokenID;

//     struct sellToken{
//         address ownerOFToken;
//         uint tokenID;
//         uint Price;
//     } 

//     uint[] public ID_ofTokenTobeSell;
//     mapping (uint => sellToken) public  tokenToBESell;
//     mapping (address => uint[]) public mappingTokenWithOwner;
//     mapping (uint => bool) public sellTheTokenCheck;

//     constructor()ERC721("Music Collection","MNFT"){
//         _tokenID = 0;
//     }
//     function mint(string memory _URI) public {
        
//         _tokenID++;
//         _mint(msg.sender, _tokenID);
//         _setTokenURI(_tokenID, _URI);
//         mappingTokenWithOwner[msg.sender].push(_tokenID);

//     }

//     function sellNFT(uint _ID,uint _price)public {
//         tokenToBESell[_ID] = sellToken({
//             ownerOFToken:msg.sender,
//             tokenID:_tokenID,
//             Price:_price
//         });
//         sellTheTokenCheck[_ID] = true;
//     }

//     function buyNFT(uint _ID)public payable {
//         // address ownerOFToken = 
//         require(sellTheTokenCheck[_ID],"token are supposed to be sell");
//         require(tokenToBESell[_ID].Price <= msg.value,"no enought money");
//         _transfer(tokenToBESell[_ID].ownerOFToken, msg.sender, _ID);
//         ChangeTheTokenOwner(mappingTokenWithOwner[tokenToBESell[_ID].ownerOFToken],_ID,tokenToBESell[_ID].ownerOFToken,msg.sender);
//         delete sellTheTokenCheck[_ID];
//         delete tokenToBESell[_ID];
//     }

//     function ChangeTheTokenOwner(uint[] memory array,uint ID,address seller,address buyer) public payable{
//         address payable payableSeller = payable (seller);
//         for(uint i = 0;i < array.length;i++){
//             if(array[i] == ID){
//                 mappingTokenWithOwner[seller][i] = array[array.length - 1];
//                 mappingTokenWithOwner[seller].pop();
//                 mappingTokenWithOwner[buyer].push(ID);
//                 payableSeller.transfer(msg.value);
//                 break ;
//             }
//         }
//     }
// }

contract MusicNFT is ERC721URIStorage{
    uint public _tokenID;

    struct sellToken{
        address ownerOFToken;
        uint tokenID;
        uint Price;
    } 

    uint[] public ID_ofTokenTobeSell;
    mapping (uint => mapping (address => bool)) public checkLikeCount;
    mapping (uint => uint) public likeCount;
    mapping (uint => sellToken) public  tokenToBESell;
    mapping (address => uint[]) public mappingTokenWithOwner;
    mapping (uint => bool) public sellTheTokenCheck;

    constructor()ERC721("Music Collection","MNFT"){
        _tokenID = 0;
    }
    function mint(string memory _URI) public {
        
        _tokenID++;
        _mint(msg.sender, _tokenID);
        _setTokenURI(_tokenID, _URI);
        mappingTokenWithOwner[msg.sender].push(_tokenID);

    }

    function sellNFT(uint _ID,uint _price)public {
        tokenToBESell[_ID] = sellToken({
            ownerOFToken:msg.sender,
            tokenID:_tokenID,
            Price:_price
        });
        sellTheTokenCheck[_ID] = true;
    }

    function buyNFT(uint _ID)public payable {
        // address ownerOFToken = 
        require(sellTheTokenCheck[_ID],"token are supposed to be sell");
        require(tokenToBESell[_ID].Price <= msg.value,"no enought money");
        _transfer(tokenToBESell[_ID].ownerOFToken, msg.sender, _ID);
        ChangeTheTokenOwner(mappingTokenWithOwner[tokenToBESell[_ID].ownerOFToken],_ID,tokenToBESell[_ID].ownerOFToken,msg.sender);
        delete sellTheTokenCheck[_ID];
        delete tokenToBESell[_ID];
    }

    function ChangeTheTokenOwner(uint[] memory array,uint ID,address seller,address buyer) public payable{
        address payable payableSeller = payable (seller);
        for(uint i = 0;i < array.length;i++){
            if(array[i] == ID){
                mappingTokenWithOwner[seller][i] = array[array.length - 1];
                mappingTokenWithOwner[seller].pop();
                mappingTokenWithOwner[buyer].push(ID);
                payableSeller.transfer(msg.value);
                break ;
            }
        }
    }
    function LikeThePost(uint ID) public {
        require(checkLikeCount[ID][msg.sender] == false,"already liked the post");
        likeCount[ID]+=1;
        checkLikeCount[ID][msg.sender] = true;
    }
}